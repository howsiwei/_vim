fun! s:DetectHaskell()
	if getline(1) == '#!/usr/bin/env stack'
		set ft=haskell
	endif
endfun

autocmd BufNewFile,BufRead * call s:DetectHaskell()
autocmd BufRead,BufNewFile *.ghci setf haskell


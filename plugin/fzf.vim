let $FZF_DEFAULT_COMMAND = 'rg --files --hidden'
	\ . ' --type-add aux:\*.aux -T aux'
	\ . ' --type-add class:\*.class -T class'
	\ . ' --type-add jpg:\*.\{jpg,jpeg\} -T jpg'
	\ . ' --type-add png:\*.png -T png'
	\ . ' --type-add obj:\*.o -T obj'
	\ . ' --type-add pdf:\*.\{pdf,PDF\} -T pdf'

command! -bang -nargs=* Rg
  \ call fzf#vim#grep(
  \   'rg --column --line-number --no-heading --color=always '.shellescape(<q-args>), 1,
  \   <bang>0 ? fzf#vim#with_preview('up:60%')
  \           : fzf#vim#with_preview('right:50%:hidden', '?'),
  \   <bang>0)


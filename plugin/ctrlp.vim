" let g:ctrlp_map = '<Leader>o'
" let g:ctrlp_show_hidden = 1
" let g:ctrlp_open_new_file = 'r'
" let g:ctrlp_default_input = 0
" let g:ctrlp_reuse_window = 'netrw\|help\|quickfix\|startify'
" if has('python3') || has('python')
" 	let g:ctrlp_match_func = { 'match': 'pymatcher#PyMatch' }
" endif
" let g:ctrlp_clear_cache_on_exit = 0
" let g:ctrlp_user_command = 'cd %s && ag -U --hidden -g ""'


if !exists("g:tmux_pane")
	let g:tmux_pane = ".1"
endif

if !exists("g:tmux_ensure_trailing_newlines")
  let g:tmux_ensure_trailing_newlines = 1
endif

function! tmux#set_pane(target)
	let g:tmux_pane = a:target
endfunction

function! tmux#send_keys(keys)
	call tbone#send_keys(g:tmux_pane, a:keys)
endfunction

function! s:ensure_newlines(lines)
	return a:lines + [""]
endfunction

function! tmux#send_line(line)
	call tmux#send_lines([a:line])
endfunction

function! tmux#send_lines(lines)
  let lines = s:ensure_newlines(a:lines)
	let keys = join(lines, "\r")
	call tbone#send_keys(g:tmux_pane, keys)
endfunction

command! -bar -nargs=? -complete=custom,tbone#complete_panes Tset
	\ execute tmux#set_pane(<q-args>)
command! -bar -nargs=? TsendKeys
	\ execute tmux#send_keys(<q-args>)
command! -bar -nargs=? TsendLine
	\ execute tmux#send_line(<q-args>)
command! -bar -nargs=? TsendLines
	\ execute tmux#send_lines(<q-args>)

set guicursor+=n-v-c:blinkon0
" set guifont=DejaVuSansMono\ Normal\ 10.5
set guioptions=c

no <C-s> :up<CR>
no! <C-s> <C-o>:up<CR>
ino <C-z> <C-o>u

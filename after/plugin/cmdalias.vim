if exists('*CmdAlias')
  call CmdAlias('h', 'tab h')
  call CmdAlias('eplug', 'e $XDG_CONFIG_HOME/nvim/plugin')
  Alias gitv Gitv
endif


syn match VarId "_"
syn match hsType "\(\[\|\]\|,\)"
syn match hsStatement		"\<\(deriving\|where\)\>"
syn match hsSpecial  "\(^\|[A-Za-z_[:space:](){}\[\],;]\)\zs\(=\||\|::\|=>\|<-\|\\\|->\)\ze\($\|[A-Za-z_[:space:](){}\[\],;]\)"
syn match hsSpecial  "\\\|@"
syn match hsConSym  "\(^\|[A-Za-z_[:space:](){}\[\],;]\)\zs\(:\)\ze\($\|[A-Za-z_[:space:](){}\[\],;]\)"
syn match hsConditional		"\<\(otherwise\)\>"


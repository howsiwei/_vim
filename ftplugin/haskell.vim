setl expandtab
" setl nosmarttab
setl shiftwidth=2 tabstop=2 softtabstop=2
nn <buffer> <silent> <F9> :up<Bar>!runhaskell %<CR>
" unlet hs_highlight_delimiters

let maplocalleader = ','
nn <buffer> <silent> <LocalLeader>b :GhciStart<CR>
nn <buffer> <silent> <LocalLeader>r :up<Bar>GhciRestart<CR>
nn <buffer> <silent> <LocalLeader>o :GhciOpen<CR>
nn <buffer> <silent> <LocalLeader>v :GhciStart<CR>:only<Bar>vsplit<Bar>buffer GHCi<CR>
nn <buffer> <silent> <LocalLeader>c :GhciHide<CR>
nn <buffer> <silent> <LocalLeader>n :GhciInfo<CR>
no <buffer> <silent> <LocalLeader>t :GhciType<CR>
nn <buffer> <silent> <LocalLeader>l :up<Bar>GhciLoadCurrentFile<CR>
nn <buffer> <silent> <LocalLeader>m :up<Bar>GhciLoadCurrentModule<CR>
nn <buffer> <silent> <LocalLeader>e :up<Bar>GhciReload<CR>
nn <buffer> <silent> <LocalLeader>d :GhciGoToDef<CR>
vn <buffer> <silent> <LocalLeader>s :GhciSend<CR>

" let g:haskell_indent_disable = 1
" let g:haskell_indent_before_where = 2
" let g:haskell_indent_after_bare_where = 2
let g:haskell_indent_if = 3

let g:ghci_command = "stack repl"
let g:ghci_start_immediately = 0

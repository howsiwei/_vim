" let g:surround_{char2nr('m')} = "\\(\r\\)"
" let g:surround_{char2nr('M')} = "\\[\r\\]"
let g:vimtex_indent_ignored_envs = ['code', 'document', 'enumerate', 'frame']

if !filereadable('Makefile')
	" setl makeprg=pdflatex\ -synctex=1\ %
	setl makeprg=latexmk\ -pdf\ %
endif
" setl formatoptions+=t

nn <buffer> <Leader>w :up<Bar>make<CR><CR>

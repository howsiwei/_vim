" if !exists("g:coquille_running") || g:coquille_running != 1
" 	call coquille#Launch()
" endif
" let g:coquille_running = 1

map <buffer> <silent> <C-Up>    :CoqUndo<CR>
map <buffer> <silent> <C-Left>  :CoqToCursor<CR>
map <buffer> <silent> <C-Down>  :CoqNext<CR>
map <buffer> <silent> <C-Right> :CoqToCursor<CR>

imap <buffer> <silent> <C-Up>    <C-\><C-o>:CoqUndo<CR>
imap <buffer> <silent> <C-Left>  <C-\><C-o>:CoqToCursor<CR>
imap <buffer> <silent> <C-Down>  <C-\><C-o>:CoqNext<CR>
imap <buffer> <silent> <C-Right> <C-\><C-o>:CoqToCursor<CR>

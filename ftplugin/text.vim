" setl spell
setl linebreak
" setl statusline=%<%F\ %h%m%r\ %=%(%{WordCount()}%)\ \ %P
setl expandtab

nn <buffer> j gj
nn <buffer> k gk

function! WordCount()
   let s:old_status = v:statusmsg
   let position = getpos(".")
   exe ":silent normal g\<c-g>"
   let stat = v:statusmsg
   let s:word_count = 0
   if stat != '--No lines in buffer--'
     let s:word_count = str2nr(split(v:statusmsg)[11])
     let v:statusmsg = s:old_status
   end
   call setpos('.', position)
   return s:word_count 
endfunction


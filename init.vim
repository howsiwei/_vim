" environment {{{1
if empty($VIM_CONFIG_HOME)
	if empty($XDG_CONFIG_HOME)
		let $XDG_CONFIG_HOME = $HOME . '/.config'
	endif
	let $VIM_CONFIG_HOME = $XDG_CONFIG_HOME . '/nvim'
endif
if empty($VIM_DATA_HOME)
	if empty($XDG_DATA_HOME)
		let $XDG_DATA_HOME = $HOME . '/.local/share'
	endif
	let $VIM_DATA_HOME = $XDG_DATA_HOME . '/nvim'
endif

" plugin {{{1
if has('vim_starting')
	" runtime! macros/matchit.vim
	set rtp+=$VIM_DATA_HOME/bundle/enabler_vim
	let g:enabler#dirs = [$VIM_DATA_HOME . '/bundle']
	runtime! plugin/enabler.vim
else
	Enableupdate
endif

Enable vim-gnupg
Enable VimCompletesMe
Enable! cmdalias.vim
Enable vim-abolish
Enable clever-f.vim
Enable vim-commentary
Enable vim-endwise
Enable vim-repeat
Enable vim-surround
Enable vim-sneak
Enable vim-unimpaired
Enablecommand vim-bbye Bdelete

Enable vim-eunuch
Enable neoformat
Enable neomake
Enable vifm.vim

Enable MatchTag
Enable base16-vim
Enable vim-colors-solarized
Enable rainbow_parentheses.vim
Enable vim-css-color

Enable ag.vim
Enable fzf
Enable fzf.vim
Enable vim-bufferline
Enable vim-filebeagle
Enablecommand tagbar TagbarToggle
Enablecommand undotree UndotreeToggle

Enable vim-tbone
Enable vim-tmux-focus-events
Enable vim-tmux-clipboard

Enable vim-fugitive
Enablecommand gitv Gitv
call enabler#Dependency('gitv', ['vim-fugitive'])
call enabler#Onload('vim-fugitive', 'let buf = bufnr("%") | bufdo doautocmd BufReadPost fugitive | exec "b".buf')

Enable voom
Enable vimtex
" Enable agda-vim
Enable haskell-vim
Enable neovim-ghci
Enable vim-scala
Enable vim-tmux
Enable vim-toml
Enable vimbufsync
Enable coquille
Enable cryptol.vim
Enable elm-vim
Enable rust.vim
Enable vim-fish
Enable vim-pentadactyl
Enable vim-nginx
Enable idris-vim
Enable nim.vim
Enable swift.vim
Enable kotlin-vim
Enable vim-solidity
Enablefiletype text vim-grammarous

" oldvim {{{1
if !has('nvim')
	set nocompatible
	set viminfo+=n~/.vim/.viminfo
	set t_vb=
endif

" set {{{1
filetype plugin indent on
set shell=/bin/bash
set completeopt+=longest
set undofile
set undodir=$VIM_DATA_HOME/undo
set backspace=2
set shiftwidth=2
set tabstop=2
set softtabstop=2
set formatoptions-=t
set history=9999
set mouse=a
set scrolloff=2
set splitright
set splitbelow
set autoindent
set hidden
" set number
" set relativenumber
set cursorline
set incsearch
set ignorecase
set smartcase
set laststatus=2
set textwidth=78
set timeoutlen=250
set updatetime=4000
set display=lastline
set gdefault
set lazyredraw
set showcmd
set tildeop
set wildmenu
" set clipboard=unnamed
set foldmethod=marker
set virtualedit+=onemore
set wildignore+=*/Trash/,*/tmp/,*/.git/,*/.hg/,*.aux,*.class,*.o,*.gz,*.jpg,*.mp4,*.ogg,*.pdf,*.png,.DS_Store
set fileencodings=utf-8,ucs-bom,gb18030,gbk,gb2312,cp936
syntax enable
set background=light
" colorscheme base16-default-light
colorscheme solarized
" colorscheme base16-solarized-light
" hi StatusLine ctermfg=0 ctermbg=8
hi WildMenu ctermfg=7 ctermbg=10
set statusline=%<%F\ %h%m%r\ %=%-12.(%l,%c%V%)\ %P
set listchars=tab:>\ ,trail:-,extends:>,precedes:<,nbsp:+
" highlight IncSearch term=standout cterm=standout ctermfg=8 ctermbg=11 gui=reverse

set cinoptions+=g0
set cinoptions+=:0
set cinoptions+=c2

" map {{{1
let mapleader = ' '
let maplocalleader = ','
no <Leader> <Nop>
no ; :
" no : ;
no @; @:
no <Down> gj
no <Up> gk
nn <C-j> <C-e>
nn <C-k> <C-y>
nn <C-y> <C-^>
nn <Leader><Tab> <C-^>
nn <Leader>` <C-^>
nn <silent> <Leader>q :q<CR>
nn <silent> <C-q> :qa<CR>
nn <silent> <Leader>x :x<CR>
nn <silent> s :up<CR>
nn <silent> <Leader>fs :up<CR>
nn <silent> <Leader>w :up<Bar>:SyntasticCheck<Bar>:Errors<CR>
nn <silent> <Leader>v :e $MYVIMRC<CR>
nn <silent> <C-n> :bn<CR>
nn <silent> <C-p> :bp<CR>
nn <silent> <C-@> :noh<CR>
nn <silent> <C-l> :noh<CR><C-l>
nn <Leader>m :up<Bar>make<CR><CR><CR>
nn mm :!def <cword><CR>
ino ij <Esc>`^
ino fd <Esc>`^
" ino <C-f> <Esc>`^
" ino <C-j> <C-o>gj
" ino <C-k> <C-o>gk
cno <C-n> <Down>
cno <C-o> <Up>
cno <C-j> <CR>
" no! <C-h> <Left>
" no! <C-l> <Right>
ino <C-a> <C-o>^
cno <C-a> <C-b>
no! <C-e> <End>
nn <silent> e @='<C-v><C-h>el'<CR>
nn <silent> E @='<C-v><C-h>El'<CR>
nn <silent> ge @='<C-v><C-h>gel'<CR>
nn <silent> gE @='<C-v><C-h>gEl'<CR>
no H ^
no L $
nn L @='$l'<CR>
nn $ @='$l'<CR>
no gH g^
no gL g$
no J L
no K H
" nn o O
" nn O o
nn <Leader>or :History<CR>

" autocmd {{{1
augroup self
	au!
	au BufWinEnter * set formatoptions-=o
	au BufEnter,BufNewFile * silent! lcd %:p:h
	au BufWritePost $MYVIMRC so %
	au BufReadPost * if line("'\"") > 1 && line("'\"") <= line("$") | exec "normal! g`\"" | endif
	" au BufReadPost,BufWrite * nn<buffer> q; q:
	au BufLeave /home/sid/Downloads/cs/**/*.in up
	au BufNewFile *.cpp,*.java,*.hs call ReadTemplate('%')
	" autocmd BufEnter * match OverLength /\%79v./
	" autocmd BufEnter * highlight OverLength ctermbg=darkgrey guibg=#592929
augroup END

" bookmark {{{1
function! Jump(char) abort
	let [bufnr, line, column, rest] = getpos("'".a:char)
	if bufnr == 0 && line > 0 || bufnr("%") == bufnr
		execute "normal! '".a:char
	elseif bufnr == 0
		echohl ErrorMsg | echom "E20: Mark not set" | echohl None
	else
		execute "edit #".bufnr
	endif
endfunction
nnoremap <silent> ' :call Jump(nr2char(getchar()))<CR>

" " bufdo {{{1
" function! BufDo(command)
" 	let currBuff=bufnr("%")
" 	execute 'bufdo '.a:command
" 	execute 'buffer '.currBuff
" endfunction
" com! -nargs=+ -complete=command Bufdo call BufDo(<q-args>)

" elm-vim {{{1
let g:elm_setup_keybindings = 0
let g:elm_classic_highlighting = 1

" gpg {{{1
function! WriteEncrypted(...)
	if argc() == 1
		let	l:filepath = argv(0)
	elseif exists('b:filepath')
		let l:filepath = b:filepath
	else
		echoerr 'No file path provided!'
	end
	execute 'cd ' . b:startdir
	execute 'w !gpg -e -o ' . l:filepath
endfunction
command! -nargs=? WriteEncrypted call WriteEncrypted(<f-args>)

" highlight {{{1
hi Delimiter ctermfg=13
hi Operator ctermfg=14
hi Search ctermfg=black
hi SpecialChar ctermfg=5
hi Structure ctermfg=9
" au! BufNewFile,BufRead *.lhs syntax match lhsBirdTrack "^>" contained

" join {{{1
nn <Leader>j :set operatorfunc=Joinoperator<CR>g@
nn <Leader>gj :set operatorfunc=GJoinoperator<CR>g@
" nn JJ Jj
func! Joinoperator(submode)
	'[,']join
endfunc
func! GJoinoperator(submode)
	'[,']join!
endfunc

" template {{{1
function! ReadTemplate(file)
	if executable('gen_template') == 1
		exec 'r !gen_template ' . a:file
		1d
	endif
endfunction

" vim-bbye {{{1
" function! BClose(bang)
" 	if &buftype != ''
" 		close
" 	else
" 		exec "Bdelete" . a:bang
" 	end
" endfunction
" command! -bang -complete=buffer -nargs=? Bclose
" 	\ :call BClose(<q-bang>)
nn <silent> <Leader>d :Bdelete<CR>
nn <silent> <Leader>D :Bdelete!<CR>

" vim-unimpaired {{{1
nmap co =o
nmap <Leader>p =p
nmap <Leader>P =P

" visualstar {{{1
function! VSetSearch()
	let temp = @s
	norm! gv"sy
	let @/ = '\V' . substitute(escape(@s, '\'), '\n', '\\n', 'g')
	let @s = temp
endfunction
xn * :<C-u>call VSetSearch()<CR>//<CR>N
xn # :<C-u>call VSetSearch()<CR>??<CR>N

" delete {{{1
function! DeleteLine(type, ...)
	exec "normal! '[V']d']+"
endfunction
nn <silent> D :set opfunc=DeleteLine<CR>g@

" yank {{{1
function! Yank(type, ...)
	if a:type == 'line'
		exec "normal! '[V']y']+"
	else
		exec "normal! `[v`]y`]l"
	endif
endfunction
nn <silent> y :set opfunc=Yank<CR>g@
nn yy yy']
xn y ygv<Esc>
function! YankLine(type, ...)
	exec "normal! '[V']y']+"
endfunction
nn <silent> Y :set opfunc=YankLine<CR>g@

" put {{{1
nn p pl
" function! Put(type, ...)
" 	if a:type == 'line'
" 		exec "normal! P']+"
" 	else
" 		exec "normal! Pl"
" 	endif
" endfunction
" nn <silent> p Pl
" function! Put1(type, ...)
" 	if a:type == 'line'
" 		exec "normal! p']"
" 	else
" 		exec "normal! p"
" 	endif
" endfunction
" nn <silent> P p

fun! JumpToDef()
  if exists("*GotoDefinition_" . &filetype)
    call GotoDefinition_{&filetype}()
  else
    exe "norm! \<C-]>"
  endif
endf

" Jump to tag
nn <M-g> :call JumpToDef()<cr>
ino <M-g> <esc>:call JumpToDef()<cr>i

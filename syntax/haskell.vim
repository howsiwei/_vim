let hs_highlight_debug = 1
hi hsSpecial ctermfg=2
hi VarId NONE
hi! link hsImport Include
hi! link hsVarSym hsOperator
hi! link hsStatement Statement
hi! link hsString String
hi! link hsStructure Structure
hi! link hsTypedef hsStructure
hi link hsConSym ConId
